import uuid

from django.contrib.auth.models import User
from django.db import models

from djmoney.models.fields import MoneyField


class Saving(models.Model):
    name = models.CharField(max_length=64)
    balance = MoneyField(max_digits=10, default_currency='IDR')
    description = models.TextField(blank=True, null=True)
    is_default = models.BooleanField(default=False)
    owner = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        if self.is_default:
            try:
                current_default = Saving.objects.get(owner=self.owner, is_default=True)
                current_default.is_default=False
                current_default.save()
            except Saving.DoesNotExist:
                self.is_default = True
            super(Saving, self).save(*args, **kwargs)
        else:
            try:
                current_default = Saving.objects.get(owner=self.owner, is_default=True)
            except Saving.DoesNotExist:
                self.is_default=True
            super(Saving, self).save(*args, **kwargs)


class Transaction(models.Model):
    TRANSACTION_CHOICES = [
        ('in', 'Income'),
        ('out', 'Expense'),
    ]
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    description = models.CharField(max_length=255)
    date = models.DateField()
    transaction_type = models.CharField(max_length=3, choices=TRANSACTION_CHOICES, default='out')
    amount = MoneyField(max_digits=9, default_currency='IDR')
    entry_by = models.ForeignKey(User, on_delete=models.CASCADE)
    saving = models.ForeignKey(Saving, on_delete=models.SET_NULL, null=True)

    def __str__(self):
        return self.description

    def save(self, *args, **kwargs):
        try:
            old_transaction = Transaction.objects.get(id=self.id)
            if self.saving is None and old_transaction.saving is not None:
                if self.transaction_type == 'in':
                    old_transaction.saving.balance = models.F('balance') - old_transaction.amount
                if self.transaction_type == 'out':
                    old_transaction.saving.balance = models.F('balance') + old_transaction.amount
                old_transaction.saving.save()
                super(Transaction, self).save(*args, **kwargs)

            if self.saving is not None and old_transaction.saving is not None:
                if self.saving.owner == self.entry_by and self.transaction_type == 'out':
                    old_transaction.saving.balance = models.F('balance') + old_transaction.amount
                    old_transaction.saving.save()
                    self.saving.balance = models.F('balance') - self.amount
                elif self.saving.owner == self.entry_by and self.transaction_type == 'in':
                    old_transaction.saving.balance = models.F('balance') - old_transaction.amount
                    old_transaction.saving.save()
                    self.saving.balance = models.F('balance') + self.amount
                else:
                    raise ValueError('Saving is not owned by user')
                self.saving.save()
                super(Transaction, self).save(*args, **kwargs)

        except Transaction.DoesNotExist:
            if self.saving is None:
                super(Transaction, self).save(*args, **kwargs)
            else:
                if self.saving.owner == self.entry_by and self.transaction_type == 'out':
                    self.saving.balance = models.F('balance') - self.amount
                elif self.saving.owner == self.entry_by and self.transaction_type == 'in':
                    self.saving.balance = models.F('balance') + self.amount
                else:
                    raise ValueError('Saving is not owned by user')
                self.saving.save()
                super(Transaction, self).save(*args, **kwargs)

