from django_filters import rest_framework as filters

from . import models


class TransactionFilter(filters.FilterSet):
    description = filters.CharFilter(lookup_expr='icontains')

    class Meta:
        model = models.Transaction
        fields = ['date', 'description', 'transaction_type', 'saving']

