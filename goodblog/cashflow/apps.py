from django.apps import AppConfig
from django.db.models import signals

from .receivers import update_saving_after_transaction_delete


class CashflowConfig(AppConfig):
    name = 'cashflow'

    def ready(self):
        signals.post_delete.connect(update_saving_after_transaction_delete, sender='cashflow.Transaction')
