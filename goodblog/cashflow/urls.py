from django.urls import path

from goodblog.urls import router

from . import views

router.register(r'cashflow/transaction', views.TransactionViewSet, basename='transaction')
router.register(r'cashflow/saving', views.SavingViewSet, basename='saving')


app_name = 'cashflow'
urlpatterns = [
    path('saving/', views.saving, name='saving'),
    path('transaction/', views.transaction, name='transaction'),
    path('', views.dashboard, name='dashboard'),
]
