from rest_framework import serializers

from . import models


class SavingSerializer(serializers.ModelSerializer):
    owner = serializers.PrimaryKeyRelatedField(read_only=True, source='owner.username')

    class Meta:
        model = models.Saving
        fields = ['id', 'name', 'is_default', 'balance', 'balance_currency', 'description', 'owner']


class TransactionSerializer(serializers.ModelSerializer):
    entry_by = serializers.PrimaryKeyRelatedField(read_only=True, source='entry_by.username')
    saving = serializers.PrimaryKeyRelatedField(read_only=True, source='saving.name')
    saving_id = serializers.IntegerField(write_only=True, allow_null=True, default=None)
    class Meta:
        model = models.Transaction
        fields = ['id', 'description', 'date', 'transaction_type', 'amount',
                  'amount_currency', 'entry_by', 'saving', 'saving_id']

