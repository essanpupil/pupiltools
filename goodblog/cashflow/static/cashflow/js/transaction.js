flatpickr('.transaction_time',
  {
    dateFormat: 'Y-m-d',
    plugins: [new confirmDatePlugin({})]
  }
)

function filterTransaction (event) {
  event.preventDefault()
  var timeFilter = document.getElementById('time-filter').value
  var descriptionFilter = document.getElementById('description-filter').value
  var typeFilter = document.getElementById('type-filter').value
  var savingFilter = document.getElementById('saving-filter').value
  var filterParams = new URLSearchParams({
    date: timeFilter,
    description: descriptionFilter,
    transaction_type: typeFilter,
    saving: savingFilter
  })

  var url = `${window.location.protocol}//${window.location.host}/api/v1/cashflow/transaction/?${filterParams.toString()}`
  checkTransactionHistory(url)
}

function resetFilterTransaction (event) {
  checkTransactionHistory()
}

function navButton (event) {
  event.preventDefault()
  checkTransactionHistory(this.href)
}

function editTransaction (transactionId) {
  var url = window.location.protocol + '//' + window.location.host + '/api/v1/cashflow/transaction/' + transactionId + '/'
  fetch(url,
    {
      method: 'GET',
      credentials: 'include',
      headers: {
        'Content-Type': 'application/json; charset=UTF-8'
      }
    }).then(function (response) {
    if (response.ok) {
      transactionForm.reset()
      return response.json()
    }
    return Promise.reject(response)
  }).then(function (data) {
    var description = document.getElementById('id_description')
    description.value = data.description

    var date = document.getElementById('id_date')
    date.value = data.date

    var transactionType = document.getElementById('id_transaction_type')
    for (var i = 0; i < transactionType.options.length; i++) {
      if (transactionType.options[i].value === data.transactionType) {
        transactionType.selectedIndex = transactionType.options[i].index
        break
      }
    }

    var amount = document.getElementById('id_amount')
    amount.value = data.amount

    var amountCurrency = document.getElementById('id_currency')
    for (var j = 0; j < amountCurrency.options.length; j++) {
      if (amountCurrency.options[j].value === data.amountCurrency) {
        amountCurrency.selectedIndex = amountCurrency.options[j].index
        break
      }
    }

    var savingId = document.getElementById('id_saving')
    for (var k = 0; k < savingId.options.length; k++) {
      if (savingId.options[k].innerText === data.saving) {
        savingId.selectedIndex = savingId.options[k].index
        break
      }
    }
    transactionForm.title = transactionId
    transactionForm.removeEventListener('submit', saveTransaction)
    transactionForm.addEventListener('submit', saveEditTransaction)
  }).catch(function (error) {
    console.warn('Something went wrong.', error)
  })
}

function saveEditTransaction (event) {
  event.preventDefault()
  var transactionId = transactionForm.title
  var description = document.getElementById('id_description').value
  var date = document.getElementById('id_date').value
  var transactionType = document.getElementById('id_transaction_type').value
  var amount = document.getElementById('id_amount').value
  var amountCurrency = document.getElementById('id_currency').value
  var savingId = document.getElementById('id_saving').value
  fetch(window.location.protocol + '//' + window.location.host + '/api/v1/cashflow/transaction/' + transactionId + '/',
    {
      method: 'PUT',
      body: JSON.stringify({
        description: description,
        date: date,
        transaction_type: transactionType,
        amount: amount,
        amount_currency: amountCurrency,
        saving_id: (savingId === '' ? null : savingId)
      }),
      credentials: 'include',
      headers: {
        'Content-Type': 'application/json; charset=UTF-8',
        'X-CSRFToken': getCookie('csrftoken')
      }
    }).then(function (response) {
    if (response.ok) {
      transactionForm.reset()
      return response.json()
    }
    return Promise.reject(response)
  }).then(function (data) {
    transactionForm.removeEventListener('submit', saveEditTransaction)
    transactionForm.addEventListener('submit', saveTransaction)
    checkTransactionHistory()
  }).catch(function (error) {
    console.warn('Something went wrong.', error)
  })
}

function deleteTransaction (transactionId) {
  var url = window.location.protocol + '//' + window.location.host + '/api/v1/cashflow/transaction/' + transactionId + '/'
  fetch(url, {
    method: 'DELETE',
    credentials: 'include',
    headers: {
      'X-CSRFToken': getCookie('csrftoken')
    }
  }).then(function (response) {
    if (response.ok) {
      var transactionTable = document.getElementById('transaction-table')
      var tableBody = transactionTable.getElementsByTagName('tbody')
      tableBody[0].innerHTML = null
      return null
    }
    return Promise.reject(response)
  }).then(function (data) {
    checkTransactionHistory()
  })
}

function saveTransaction (event) {
  event.preventDefault()
  var description = document.getElementById('id_description').value
  var date = document.getElementById('id_date').value
  var transactionType = document.getElementById('id_transaction_type').value
  var amount = document.getElementById('id_amount').value
  var amountCurrency = document.getElementById('id_currency').value
  var savingId = document.getElementById('id_saving').value
  fetch(window.location.protocol + '//' + window.location.host + '/api/v1/cashflow/transaction/',
    {
      method: 'POST',
      body: JSON.stringify({
        description: description,
        date: date,
        transaction_type: transactionType,
        amount: amount,
        amount_currency: amountCurrency,
        saving_id: (savingId === '' ? null : savingId)
      }),
      credentials: 'include',
      headers: {
        'Content-Type': 'application/json; charset=UTF-8',
        'X-CSRFToken': getCookie('csrftoken')
      }
    }).then(function (response) {
    if (response.ok) {
      transactionForm.reset()
      return response.json()
    }
    return Promise.reject(response)
  }).then(function (data) {
    checkTransactionHistory()
  }).catch(function (error) {
    console.warn('Something went wrong.', error)
  })
}

function checkTransactionHistory (url=window.location.protocol + '//' + window.location.host + '/api/v1/cashflow/transaction/') {
  fetch(url, {
    credentials: 'include',
    headers: {
      'Content-Type': 'application/json; charset=UTF-8'
    }
  }).then(function (response) {
    if (response.ok) {
      return response.json()
    }
    return Promise.reject(response)
  }).then(function (data) {
    if (data.count > 0) {
      populateTransactionTable(data)
    } else {
      var transactionTable = document.getElementById('transaction-table')
      var tableBody = transactionTable.getElementsByTagName('tbody')
      tableBody[0].innerHTML = '<tr><td> - </td><td> - </td><td> - </td><td> - </td><td> - </td></tr>'
    }
  }).catch(function (error) {
    console.warn('Something went wrong.', error)
  })
}

function populateTransactionTable (data) {
  var transactionTable = document.getElementById('transaction-table')
  var tableBody = transactionTable.getElementsByTagName('tbody')
  tableBody[0].innerHTML = null

  for (var i = 0; i < data.results.length; i++) {
    var row = document.createElement('tr')
    row.id = 'transaction_' + data.results[i].id

    var timeData = document.createElement('td')
    timeData.innerHTML = data.results[i].date
    row.appendChild(timeData)

    var descriptionData = document.createElement('td')
    descriptionData.innerHTML = data.results[i].description
    row.appendChild(descriptionData)

    var amountData = document.createElement('td')
    amountData.innerHTML = parseFloat(data.results[i].amount).toLocaleString('id')
    amountData.style.textAlign = 'right'
    if (data.results[i].transaction_type === 'in') {
      amountData.style.color = 'blue'
    }
    row.appendChild(amountData)

    var savingData = document.createElement('td')
    savingData.innerHTML = data.results[i].saving
    row.appendChild(savingData)

    var toolsData = document.createElement('td')

    var editButton = document.createElement('BUTTON')
    editButton.id = data.results[i].id
    editButton.innerHTML = 'E'
    editButton.title = 'Edit'
    editButton.classList.add('button')
    editButton.classList.add('is-small')
    editButton.classList.add('is-info')
    editButton.addEventListener('click', function (event) {
      event.preventDefault()
      editTransaction(this.id)
    })
    toolsData.appendChild(editButton)

    var deleteButton = document.createElement('BUTTON')
    deleteButton.id = data.results[i].id
    deleteButton.innerHTML = 'X'
    deleteButton.title = 'Hapus ' + data.results[i].description
    deleteButton.classList.add('button')
    deleteButton.classList.add('is-small')
    deleteButton.classList.add('is-danger')
    deleteButton.addEventListener('click', function (event) {
      event.preventDefault()
      deleteTransaction(this.id)
    })
    toolsData.appendChild(deleteButton)

    row.appendChild(toolsData)
    tableBody[0].appendChild(row)
  }
  var nextButton = document.getElementById('next-button')
  nextButton.removeEventListener('click', navButton)
  nextButton.href = data.next
  nextButton.addEventListener('click', navButton)

  var previousButton = document.getElementById('previous-button')
  previousButton.removeEventListener('click', navButton)
  previousButton.href = data.previous
  previousButton.addEventListener('click', navButton)
}

checkTransactionHistory()

function checkSavingList () {
  fetch(window.location.protocol + '//' + window.location.host + '/api/v1/cashflow/saving/', {
    credentials: 'include',
    headers: {
      'Content-Type': 'application/json; charset=UTF-8'
    }
  }).then(function (response) {
    if (response.ok) {
      return response.json()
    }
    return Promise.reject(response)
  }).then(function (data) {
    if (data.count > 0) {
      populateSavingOptions(data)
    }
  }).catch(function (error) {
    console.warn('Something went wrong.', error)
  })
}

function populateSavingOptions (data) {
  var savingSelects = document.getElementsByClassName('savingSelect')
  for (var a = 0; a < savingSelects.length; a++) {
    if (data.results.length > 0) {
      savingSelects[a].innerHTML = null
    }

    for (var i = 0; i < data.results.length; i++) {
      var option = document.createElement('option')
      option.value = data.results[i].id
      option.innerHTML = data.results[i].name
      savingSelects[a].appendChild(option)
    }
  }
}

checkSavingList()

var transactionForm = document.getElementById('transaction-form')
transactionForm.addEventListener('submit', saveTransaction)

var filterForm = document.getElementById('filter-form')
filterForm.addEventListener('submit', filterTransaction)
filterForm.addEventListener('reset', resetFilterTransaction)
