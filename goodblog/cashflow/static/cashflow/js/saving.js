function editSaving (savingID) {
  var url = window.location.protocol + '//' + window.location.host + '/api/v1/cashflow/saving/' + savingID + '/'
  fetch(url,
    {
      method: 'GET',
      credentials: 'include',
      headers: {
        'Content-Type': 'application/json; charset=UTF-8'
      }
    }).then(function (response) {
    if (response.ok) {
      savingForm.reset()
      return response.json()
    }
    return Promise.reject(response)
  }).then(function (data) {
    var name = document.getElementById('id_name')
    name.value = data.name

    var balance = document.getElementById('id_amount')
    balance.value = data.balance

    var balanceCurrency = document.getElementById('id_currency')
    for (var i = 0; i < balanceCurrency.options.length; i++) {
      if (balanceCurrency.options[i].value === data.balance_currency) {
        balanceCurrency.selectedIndex = balanceCurrency.options[i].index
        break
      }
    }

    var isDefault = document.getElementById('id_is_default')
    isDefault.checked = data.isDefault

    var description = document.getElementById('id_description')
    description.value = data.description

    savingForm.title = savingID
    savingForm.removeEventListener('submit', saveSaving)
    savingForm.addEventListener('submit', saveEditSaving)
  }).catch(function (error) {
    console.warn('Something went wrong.', error)
  })
}

function saveEditSaving (event) {
  event.preventDefault()
  var savingID = savingForm.title
  var name = document.getElementById('id_name').value
  var currency = document.getElementById('id_currency').value
  var balance = document.getElementById('id_amount').value
  var isDefault = document.getElementById('id_is_default').checked
  var description = document.getElementById('id_description').value
  fetch(window.location.protocol + '//' + window.location.host + '/api/v1/cashflow/saving/' + savingID + '/',
    {
      method: 'PUT',
      body: JSON.stringify({
        name: name,
        amount_currency: currency,
        balance: balance,
        is_default: isDefault,
        description: description
      }),
      credentials: 'include',
      headers: {
        'Content-Type': 'application/json; charset=UTF-8',
        'X-CSRFToken': getCookie('csrftoken')
      }
    }).then(function (response) {
    if (response.ok) {
      savingForm.reset()
      return response.json()
    }
    return Promise.reject(response)
  }).then(function (data) {
    savingForm.removeEventListener('submit', saveEditSaving)
    savingForm.addEventListener('submit', saveSaving)
    checkSavingList()
  }).catch(function (error) {
    console.warn('Something went wrong.', error)
  })
}

function saveSaving (event) {
  event.preventDefault()
  var name = document.getElementById('id_name').value
  var currency = document.getElementById('id_currency').value
  var balance = document.getElementById('id_amount').value
  var isDefault = document.getElementById('id_is_default').checked
  var description = document.getElementById('id_description').value
  fetch(window.location.protocol + '//' + window.location.host + '/api/v1/cashflow/saving/', {
    method: 'POST',
    body: JSON.stringify({
      name: name,
      amount_currency: currency,
      balance: balance,
      is_default: isDefault,
      description: description
    }),
    credentials: 'include',
    headers: {
      'Content-Type': 'application/json; charset=UTF-8',
      'X-CSRFToken': getCookie('csrftoken')
    }
  }).then(function (response) {
    if (response.ok) {
      document.getElementById('saving-form').reset()
      return response.json()
    }
    return Promise.reject(response)
  }).then(function (data) {
    checkSavingList()
  }).catch(function (error) {
    console.warn('Something went wrong.', error)
  })
}

function deleteSaving (savingId) {
  var url = window.location.protocol + '//' + window.location.host + '/api/v1/cashflow/saving/' + savingId + '/'
  fetch(url, {
    method: 'DELETE',
    credentials: 'include',
    headers: {
      'X-CSRFToken': getCookie('csrftoken')
    }
  }).then(function (response) {
    if (response.ok) {
      var savingTable = document.getElementById('saving-table')
      var tableBody = savingTable.getElementsByTagName('tbody')
      tableBody[0].innerHTML = null
      return null
    }
    return Promise.reject(response)
  }).then(function (data) {
    checkSavingList()
  })
}

function checkSavingList () {
  fetch(window.location.protocol + '//' + window.location.host + '/api/v1/cashflow/saving/', {
    credentials: 'include',
    headers: {
      'Content-Type': 'application/json; charset=UTF-8'
    }
  }).then(function (response) {
    if (response.ok) {
      return response.json()
    }
    return Promise.reject(response)
  }).then(function (data) {
    if (data.count > 0) {
      populateSavingTable(data)
    }
  }).catch(function (error) {
    console.warn('Something went wrong.', error)
  })
}

function populateSavingTable (data) {
  var savingTable = document.getElementById('saving-table')
  var tableBody = savingTable.getElementsByTagName('tbody')
  tableBody[0].innerHTML = null

  for (var i = 0; i < data.results.length; i++) {
    var row = document.createElement('tr')

    var numberData = document.createElement('td')
    numberData.innerHTML = i + 1
    row.appendChild(numberData)

    var nameData = document.createElement('td')
    nameData.innerHTML = data.results[i].name
    row.appendChild(nameData)

    var isDefaultData = document.createElement('td')
    isDefaultData.innerHTML = data.results[i].is_default
    row.appendChild(isDefaultData)

    var currencyData = document.createElement('td')
    currencyData.innerHTML = data.results[i].balance_currency
    row.appendChild(currencyData)

    var balanceData = document.createElement('td')
    balanceData.innerHTML = parseFloat(data.results[i].balance).toLocaleString('id')
    balanceData.style.textAlign = 'right'
    row.appendChild(balanceData)

    var toolData = document.createElement('td')
    var editButton = document.createElement('button')
    editButton.id = data.results[i].id
    editButton.innerHTML = 'E'
    editButton.title = 'Edit'
    editButton.classList.add('button')
    editButton.classList.add('is-small')
    editButton.classList.add('is-info')
    editButton.addEventListener('click', function (event) {
      event.preventDefault()
      editSaving(this.id)
    })
    toolData.appendChild(editButton)

    var deleteButton = document.createElement('button')
    deleteButton.id = data.results[i].id
    deleteButton.innerHTML = 'X'
    deleteButton.title = 'Hapus'
    deleteButton.classList.add('button')
    deleteButton.classList.add('is-small')
    deleteButton.classList.add('is-warning')
    deleteButton.addEventListener('click', function (event) {
      event.preventDefault()
      deleteSaving(this.id)
    })
    toolData.appendChild(deleteButton)
    row.appendChild(toolData)

    tableBody[0].appendChild(row)
  }
}

checkSavingList()

var savingForm = document.getElementById('saving-form')
savingForm.addEventListener('submit', saveSaving)
