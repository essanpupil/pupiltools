from django.contrib.auth.decorators import login_required
from django.shortcuts import render

from rest_framework.viewsets import ModelViewSet
from django_filters.rest_framework import DjangoFilterBackend

from . import models, serializers, permissions, filters

@login_required
def dashboard(request):
    return render(request, 'cashflow/dashboard.html')


@login_required
def transaction(request):
    return render(request, 'cashflow/transaction.html')


@login_required
def saving(request):
    return render(request, 'cashflow/saving.html')


class SavingViewSet(ModelViewSet):
    serializer_class = serializers.SavingSerializer
    permission_classes = (
        permissions.SavingAccessPolicy,
    )

    def get_queryset(self):
        return models.Saving.objects.filter(owner=self.request.user).order_by('-id')

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)


class TransactionViewSet(ModelViewSet):
    filter_backends = [DjangoFilterBackend]
    filterset_class = filters.TransactionFilter
    serializer_class = serializers.TransactionSerializer
    permission_classes = (
        permissions.TransactionAccessPolicy,
    )

    def get_queryset(self):
        return models.Transaction.objects.filter(
            entry_by=self.request.user).order_by('-date', '-id')

    def perform_create(self, serializer):
        serializer.save(entry_by=self.request.user)
