from rest_access_policy import AccessPolicy
from djmoney.money import Money


class SavingAccessPolicy(AccessPolicy):
    statements = [
        {
            "action": ["list", "create", "retrieve"],
            "principal": "authenticated",
            "effect": "allow",
        },
        {
            "action": ["destroy"],
            "principal": "authenticated",
            "effect": "allow",
            "condition": ["is_owner"]
        },
        {
            "action": ["update"],
            "principal": "authenticated",
            "effect": "allow",
            "condition": ["is_owner", "not_balance"]
        }
    ]

    def is_owner(self, request, view, action) -> bool:
        saving = view.get_object()
        return request.user == saving.owner

    def not_balance(self, request, view, action) -> bool:
        saving = view.get_object()
        balance_unchanged = saving.balance == Money(request.data.get('balance'), 'IDR')
        balance_data_empty = 'balance' not in request.data.keys()
        return balance_unchanged or balance_data_empty


class TransactionAccessPolicy(AccessPolicy):
    statements = [
        {
            "action": ["list", "create", "retrieve"],
            "principal": "authenticated",
            "effect": "allow",
        },
        {
            "action": ["destroy", "update"],
            "principal": "authenticated",
            "effect": "allow",
            "condition": "is_entry_by",
        }
    ]

    def is_entry_by(self, request, view, action) -> bool:
        transaction = view.get_object()
        return request.user == transaction.entry_by

