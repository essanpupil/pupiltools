from django.urls import reverse

from . import factories
from .api_base import APIBaseTestCase


class GetSavingTest(APIBaseTestCase):

    def test_get_savinglist_not_login(self):
        url = reverse('saving-list')
        response = self.client.get(url)
        self.assertEqual(403, response.status_code)

    def test_get_saving_list_owner(self):
        user1 = self.create_user(username='fulan', password='unknownman')
        saving = factories.SavingFactory(owner=user1)
        self.client.login(username=user1.username, password='unknownman')

        url = reverse('saving-list')
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)
        self.assertEqual(response.data['count'], 1, response.content)
        self.assertEqual(
            response.data.get('results')[0]['name'],
            saving.name,
        )

    def test_get_saving_list_non_owner(self):
        user1 = self.create_user()
        factories.SavingFactory(owner=user1)

        user2 = self.create_user(username='polan', password='unknownman')
        self.client.login(username=user2.username, password='unknownman')

        url = reverse('saving-list')
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)
        self.assertEqual(response.data['count'], 0, response.data)

    def test_get_saving_detail_owner(self):
        user1 = self.create_user(username='fulan', password='unknownman')
        saving = factories.SavingFactory(owner=user1)
        self.client.login(username=user1.username, password='unknownman')

        url = reverse('saving-detail', args=[saving.id])
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)
        self.assertEqual(response.data['name'], saving.name)

    def test_get_saving_detail_non_owner(self):
        user1 = self.create_user()
        saving = factories.SavingFactory(owner=user1)

        user2 = self.create_user(username='polan', password='unknownman')
        self.client.login(username=user2.username, password='unknownman')

        url = reverse('saving-detail', args=[saving.id])
        response = self.client.get(url)
        self.assertEqual(404, response.status_code)

