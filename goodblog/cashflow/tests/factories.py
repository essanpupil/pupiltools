from django.utils import timezone

import factory

from cashflow import models
from djcore.tests.factories import UserFactory


class TransactionFactory(factory.DjangoModelFactory):
    class Meta:
        model = models.Transaction

    description = 'Testing transaction'
    date = timezone.now()
    amount = 100000
    entry_by = factory.SubFactory(UserFactory)


class SavingFactory(factory.DjangoModelFactory):
    class Meta:
        model = models.Saving

    name = 'Test Saving'
    balance = 100000
    owner = factory.SubFactory(UserFactory)

