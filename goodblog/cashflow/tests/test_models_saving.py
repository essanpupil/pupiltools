from django.test import TestCase
from cashflow import models
from cashflow.tests import factories
from djcore.tests import factories as core_factories


class SavingModelTest(TestCase):
    def test_save_one_saving_not_default(self):
        user = core_factories.UserFactory()
        models.Saving.objects.create(
                name='Dummy',
                description="Dummy saving account",
                balance=200000,
                is_default=False,
                owner=user,
                )
        self.assertEqual(1, models.Saving.objects.all().count())
        self.assertTrue(models.Saving.objects.get(name='Dummy').is_default)

    def test_save_one_saving_default(self):
        user = core_factories.UserFactory()
        models.Saving.objects.create(
                name='Dummy',
                description="Dummy saving account",
                balance=200000,
                is_default=True,
                owner=user,
                )
        self.assertEqual(1, models.Saving.objects.all().count())
        self.assertTrue(models.Saving.objects.get(name='Dummy').is_default)

    def test_save_two_saving(self):
        user = core_factories.UserFactory()
        models.Saving.objects.create(
                name='Dummy One',
                description="Dummy saving account",
                balance=200000,
                owner=user,
                )
        self.assertEqual(1, models.Saving.objects.all().count())
        self.assertTrue(models.Saving.objects.get(name='Dummy One').is_default)

        models.Saving.objects.create(
                name='Dummy Two',
                description="Dummy saving account",
                balance=200000,
                owner=user,
                )
        self.assertEqual(2, models.Saving.objects.all().count())
        self.assertFalse(models.Saving.objects.get(name='Dummy Two').is_default)

    def test_save_two_default_saving(self):
        user = core_factories.UserFactory()
        models.Saving.objects.create(
                name='Dummy One',
                description="Dummy saving account",
                balance=200000,
                owner=user,
                is_default=True
                )
        self.assertEqual(1, models.Saving.objects.all().count())
        self.assertTrue(models.Saving.objects.get(name='Dummy One').is_default)

        models.Saving.objects.create(
                name='Dummy Two',
                description="Dummy saving account",
                balance=200000,
                owner=user,
                is_default=True
                )
        self.assertEqual(2, models.Saving.objects.all().count())
        self.assertTrue(models.Saving.objects.get(name='Dummy Two').is_default)
        self.assertFalse(models.Saving.objects.get(name='Dummy One').is_default)

    def test_edit_saving(self):
        saving = factories.SavingFactory(name='Dummy One')
        self.assertEqual(1, models.Saving.objects.all().count())
        self.assertEqual(models.Saving.objects.get(id=saving.id).name, 'Dummy One')

        saving.name = 'Dummy Two'
        saving.save()
        self.assertEqual(1, models.Saving.objects.all().count())
        self.assertEqual(models.Saving.objects.get(id=saving.id).name, 'Dummy Two')

    def test_delete_saving(self):
        saving = factories.SavingFactory(name='Dummy One')
        self.assertEqual(1, models.Saving.objects.all().count())
        self.assertEqual(models.Saving.objects.get(id=saving.id).name, 'Dummy One')

        models.Saving.objects.get(id=saving.id).delete()
        self.assertEqual(0, models.Saving.objects.all().count())

    def test_delete_saving_with_transaction(self):
        user = core_factories.UserFactory()
        saving = factories.SavingFactory(name='Dummy One', owner=user)
        factories.TransactionFactory(saving=saving, entry_by=user)
        self.assertEqual(1, models.Saving.objects.all().count())
        self.assertEqual(models.Saving.objects.get(id=saving.id).name, 'Dummy One')

        models.Saving.objects.get(id=saving.id).delete()
        self.assertEqual(0, models.Saving.objects.all().count())
