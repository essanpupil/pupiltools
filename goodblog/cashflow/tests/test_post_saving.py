from django.urls import reverse

from .test_get_cashflow import APIBaseTestCase
from cashflow.tests import factories


class PostSavingTest(APIBaseTestCase):
    def test_post_saving(self):
        user1 = self.create_user(username='fulan', password='unknownman')
        self.client.login(username=user1.username, password='unknownman')

        response = self.client.post(
            reverse('saving-list'),
            {
                'name': 'Test account',
                'description': 'Test account description',
                'balance': 200000,
                'balance_currency': 'IDR',
            },
        )
        self.assertEqual(201, response.status_code, response.data)

    def test_edit_saving(self):
        user = self.create_user(username='fulan', password='unknownman')
        saving = factories.SavingFactory(
                name='Old saving name',
                description='Saving for old time',
                balance=1000,
                balance_currency='IDR',
                owner=user
                )
        self.client.login(username=user.username, password='unknownman')

        response = self.client.put(
            reverse('saving-detail', args=[saving.id]),
            {
                'name': 'New saving name',
                'description': 'Test account description',
                'balance': 1000,
                'balance_currency': 'IDR',
            },
        )
        self.assertEqual(200, response.status_code, response.data)
        self.assertEqual(response.data['name'], 'New saving name')

    def test_delete_saving(self):
        user = self.create_user(username='fulan', password='unknownman')
        saving = factories.SavingFactory(
                name='Old saving name',
                description='Saving for old time',
                balance=1000,
                balance_currency='IDR',
                owner=user
                )
        self.client.login(username=user.username, password='unknownman')

        response = self.client.delete(reverse('saving-detail', args=[saving.id]))
        self.assertEqual(204, response.status_code, response.data)

    def test_edit_balance_saving(self):
        user = self.create_user(username='fulan', password='unknownman')
        saving = factories.SavingFactory(name='Old saving name', owner=user, balance=100)
        self.client.login(username=user.username, password='unknownman')

        response = self.client.put(
            reverse('saving-detail', args=[saving.id]),
            {
                'name': 'New saving name',
                'description': 'Test account description',
                'balance': 300,
                'balance_currency': 'IDR',
            },
        )
        self.assertEqual(403, response.status_code, response.data)

    def test_edit_saving_non_owner(self):
        user1 = self.create_user(username='fulan', password='unknownman')
        user2 = self.create_user(username='foolan', password='unknownman')
        saving = factories.SavingFactory(name='Old saving name', owner=user1)
        self.client.login(username=user2.username, password='unknownman')

        response = self.client.put(
            reverse('saving-detail', args=[saving.id]),
            {
                'name': 'New saving name',
                'description': 'Test account description',
                'balance_currency': 'IDR',
            },
        )
        self.assertEqual(404, response.status_code, response.data)

