from django.urls import reverse

from djcore.tests.factories import UserFactory
from . import factories
from .api_base import APIBaseTestCase


class GetTransactionTest(APIBaseTestCase):

    def test_get_transaction_list_not_login(self):
        url = reverse('transaction-list')
        response = self.client.get(url)
        self.assertEqual(403, response.status_code)

    def test_get_transaction_detail_author(self):
        user1 = self.create_user(username='fulan', password='unknownman')
        transaction = factories.TransactionFactory(entry_by=user1)
        self.client.login(username=user1.username, password='unknownman')

        url = reverse('transaction-detail', args=[transaction.id,])
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)
        self.assertEqual(
            response.data['description'],
            transaction.description,
            response.data
        )

    def test_get_transaction_list_author(self):
        user1 = self.create_user(username='fulan', password='unknownman')
        transaction = factories.TransactionFactory(entry_by=user1)
        self.client.login(username=user1.username, password='unknownman')

        url = reverse('transaction-list')
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)
        self.assertEqual(response.data['count'], 1)
        self.assertEqual(
            response.data.get('results')[0]['description'],
            transaction.description,
        )

    def test_get_transaction_list_non_author(self):
        user1 = self.create_user()
        factories.TransactionFactory(entry_by=user1)

        user2 = self.create_user(username='polan', password='unknownman')
        self.client.login(username=user2.username, password='unknownman')

        url = reverse('transaction-list')
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)
        self.assertEqual(response.data['count'], 0, response.data)

