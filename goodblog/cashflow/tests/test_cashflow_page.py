from django.urls import reverse

from djcore.tests.base import BaseTest

class CashFlowPageTest(BaseTest):
    def test_page_non_login(self):
        response = self.client.get(reverse('cashflow:dashboard'))
        self.assertEqual(response.status_code, 302)

    def test_page_login(self):
        user = self.create_user()
        self.user_login(user, 'testpassword')
        response = self.client.get(reverse('cashflow:dashboard'))
        self.assertEqual(response.status_code, 200)


class TransactionPageTest(BaseTest):
    def test_page_non_login(self):
        response = self.client.get(reverse('cashflow:transaction'))
        self.assertEqual(response.status_code, 302)

    def test_page_login(self):
        user = self.create_user()
        self.user_login(user, 'testpassword')
        response = self.client.get(reverse('cashflow:transaction'))
        self.assertEqual(response.status_code, 200)


class SavingsPageTest(BaseTest):
    def test_page_non_login(self):
        response = self.client.get(reverse('cashflow:saving'))
        self.assertEqual(response.status_code, 302)

    def test_page_login(self):
        user = self.create_user()
        self.user_login(user, 'testpassword')
        response = self.client.get(reverse('cashflow:saving'))
        self.assertEqual(response.status_code, 200)
