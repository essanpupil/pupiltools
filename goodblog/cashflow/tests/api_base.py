from rest_framework.test import APILiveServerTestCase

from djcore.tests.factories import UserFactory

class APIBaseTestCase(APILiveServerTestCase):

    def create_user(self, username='fulan', password='testpasswd'):
        user = UserFactory(username=username, password=password)
        return user

