from django.urls import reverse
from django.utils import timezone

from djmoney.money import Money

from djcore.tests import factories as core_factories

from .test_get_cashflow import APIBaseTestCase
from .factories import SavingFactory, TransactionFactory
from cashflow import models


class PostTransactionTest(APIBaseTestCase):
    def test_post_transaction(self):
        user1 = self.create_user(username='fulan', password='unknownman')
        self.client.login(username=user1.username, password='unknownman')

        response = self.client.post(
            reverse('transaction-list'),
            {
                'description': 'Test buy laptop',
                'date': timezone.now().strftime("%Y-%m-%d"),
                'transaction_type': 'out',
                'amount': 200000,
                'amount_currency': 'IDR',
            },
        )
        self.assertEqual(201, response.status_code, response.data)
        self.assertEqual(response.data['entry_by'], user1.username)

    def test_post_transaction_with_others_default_saving(self):
        user1 = self.create_user(username='fulan', password='unknownman')
        saving = SavingFactory(is_default=True, owner=user1)

        user2 = self.create_user(username='fulana', password='unknownman')
        self.client.login(username=user2.username, password='unknownman')

        with self.assertRaises(ValueError):
            response = self.client.post(
                reverse('transaction-list'),
                {
                    'description': 'Test buy laptop',
                    'date': timezone.now().strftime("%Y-%m-%d"),
                    'transaction_type': 'out',
                    'amount': 200000,
                    'amount_currency': 'IDR',
                    'saving_id': saving.id
                },
            )

    def test_update_transaction_change_saving(self):
        user = self.create_user(username='fulan', password='unknownman')
        saving1 = SavingFactory(name='Save One', owner=user, is_default=True, balance=100)
        saving2 = SavingFactory(name='Save Two', owner=user, is_default=False, balance=100)
        transaction = TransactionFactory(
                transaction_type='out',
                amount=20,
                entry_by=user,
                saving=saving1
        )
        self.client.login(username=user.username, password='unknownman')
        self.assertEqual(models.Saving.objects.get(id=saving1.id).balance, Money(80, 'IDR'))

        response = self.client.put(
            reverse('transaction-detail', args=[transaction.id]),
            {
                'description': 'Test buy laptop',
                'date': timezone.now().strftime("%Y-%m-%d"),
                'transaction_type': 'out',
                'amount': 10,
                'amount_currency': 'IDR',
                'saving_id': saving2.id,
            },
        )
        self.assertEqual(200, response.status_code, response.data)
        self.assertEqual(models.Saving.objects.get(id=saving1.id).balance, Money(100, 'IDR'))
        self.assertEqual(models.Saving.objects.get(id=saving2.id).balance, Money(90, 'IDR'))

    def test_update_transaction(self):
        user = self.create_user(username='fulan', password='unknownman')
        saving = SavingFactory(owner=user, is_default=True, balance=100)
        transaction = TransactionFactory(
                transaction_type='out',
                amount=20,
                entry_by=user,
                saving=saving
        )
        self.client.login(username=user.username, password='unknownman')
        self.assertEqual(models.Saving.objects.get(id=saving.id).balance, Money(80, 'IDR'))

        response = self.client.put(
            reverse('transaction-detail', args=[transaction.id]),
            {
                'description': 'Test buy laptop',
                'date': timezone.now().strftime("%Y-%m-%d"),
                'transaction_type': 'out',
                'amount': 10,
                'amount_currency': 'IDR',
                'saving_id': saving.id,
            },
        )
        self.assertEqual(200, response.status_code, response.data)
        self.assertEqual(models.Saving.objects.get(id=saving.id).balance, Money(90, 'IDR'))

    def test_post_transaction_with_my_default_saving(self):
        user1 = self.create_user(username='fulan', password='unknownman')
        saving = SavingFactory(is_default=True, owner=user1)
        self.client.login(username=user1.username, password='unknownman')

        response = self.client.post(
            reverse('transaction-list'),
            {
                'description': 'Test buy laptop',
                'date': timezone.now().strftime("%Y-%m-%d"),
                'transaction_type': 'out',
                'amount': 200000,
                'amount_currency': 'IDR',
                'saving_id': saving.id,
            },
        )
        self.assertEqual(201, response.status_code, response.data)
        self.assertEqual(response.data['saving'], saving.name)

    def test_post_transaction_without_default_saving(self):
        saving = SavingFactory()
        user1 = self.create_user(username='fulan', password='unknownman')
        self.assertEqual(saving.owner, user1)

        self.client.login(username=user1.username, password='unknownman')
        response = self.client.post(
            reverse('transaction-list'),
            {
                'description': 'Test buy laptop',
                'date': timezone.now().strftime("%Y-%m-%d"),
                'transaction_type': 'out',
                'amount': 200000,
                'amount_currency': 'IDR',
                'saving_id': saving.id
            },
        )
        self.assertEqual(201, response.status_code, response.data)
        self.assertEqual(response.data['saving'], saving.name)

    def test_save_transaction_with_gibberish_saving_value(self):
        user1 = self.create_user(username='fulan', password='unknownman')
        self.client.login(username=user1.username, password='unknownman')

        with self.assertRaises(models.Saving.DoesNotExist):
            response = self.client.post(
                reverse('transaction-list'),
                {
                    'description': 'Test buy laptop',
                    'date': timezone.now().strftime("%Y-%m-%d"),
                    'transaction_type': 'out',
                    'amount': 200000,
                    'amount_currency': 'IDR',
                    'saving_id': 45
                },
            )

    def test_delete_transaction(self):
        user = core_factories.UserFactory(username='fulan', password='unknownman')
        saving = SavingFactory(owner=user, is_default=True, balance=100)
        transaction = TransactionFactory(entry_by=user, saving=saving)

        self.client.login(username=user.username, password='unknownman')
        response = self.client.delete(reverse('transaction-detail', args=[transaction.id]))
        self.assertEqual(204, response.status_code, response.data)
        self.assertFalse(models.Transaction.objects.filter(id=transaction.id).exists())


    def test_delete_transactioni_non_owner(self):
        user = core_factories.UserFactory(username='fulan', password='unknownman')
        user2 = core_factories.UserFactory(username='fulana', password='unknownman')
        saving = SavingFactory(owner=user, is_default=True, balance=100)
        transaction = TransactionFactory(entry_by=user, saving=saving)

        self.client.login(username=user2.username, password='unknownman')
        response = self.client.delete(reverse('transaction-detail', args=[transaction.id]))
        self.assertEqual(404, response.status_code, response.data)

