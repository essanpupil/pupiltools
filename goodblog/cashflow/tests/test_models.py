from django.test import TestCase
from django.utils import timezone

from djmoney.money import Money

from cashflow import models
from cashflow.tests import factories

from djcore.tests import factories as core_factories


class TransactionModelTest(TestCase):
    def test_save_transaction_without_saving(self):
        user = core_factories.UserFactory()
        transaction = models.Transaction.objects.create(
                description="Dummy transaction",
                date=timezone.now(),
                amount='200000',
                entry_by=user,
                )
        self.assertEqual(models.Transaction.objects.get(id=transaction.id).saving, None)

    def test_save_transaction_with_others_default_saving(self):
        user1 = core_factories.UserFactory(username='johndoe')
        saving = factories.SavingFactory(owner=user1, is_default=True)

        user = core_factories.UserFactory()
        with self.assertRaises(ValueError):
            transaction = models.Transaction.objects.create(
                    description="Dummy transaction",
                    date=timezone.now(),
                    amount='200000',
                    entry_by=user,
                    saving=saving,
                    )

    def test_save_transaction_with_my_default_saving(self):
        user = core_factories.UserFactory()
        saving = factories.SavingFactory(owner=user, is_default=True)

        transaction = models.Transaction.objects.create(
                description="Dummy transaction",
                date=timezone.now(),
                amount='200000',
                entry_by=user,
                saving=saving,
                )
        self.assertEqual(models.Transaction.objects.get(id=transaction.id).saving, saving)

    def test_save_transaction_with_my_non_default_saving(self):
        user = core_factories.UserFactory()
        saving1 = factories.SavingFactory(name='Account One', owner=user, is_default=False)
        self.assertTrue(models.Saving.objects.get(id=saving1.id).is_default)

        saving2 = factories.SavingFactory(name='Account Two', owner=user, is_default=False)
        self.assertTrue(models.Saving.objects.get(id=saving1.id).is_default)
        self.assertFalse(models.Saving.objects.get(id=saving2.id).is_default)

        transaction = models.Transaction.objects.create(
                description="Dummy transaction",
                date=timezone.now(),
                amount='200000',
                entry_by=user,
                saving=saving2
                )
        self.assertEqual(models.Transaction.objects.get(id=transaction.id).saving, saving2)
        self.assertFalse(models.Transaction.objects.get(id=transaction.id).saving.is_default)

    def test_update_transaction_in_decrease_saving(self):
        user = core_factories.UserFactory()
        saving = factories.SavingFactory(owner=user, is_default=True, balance=100)

        transaction = models.Transaction.objects.create(
                description="Dummy transaction",
                date=timezone.now(),
                transaction_type='in',
                amount=20,
                entry_by=user,
                saving=saving
                )
        self.assertEqual(models.Transaction.objects.get(id=transaction.id).saving, saving)
        self.assertEqual(models.Saving.objects.get(id=saving.id).balance, Money(120, 'IDR'))

        transaction = models.Transaction.objects.get(id=transaction.id)
        transaction.amount = 10
        transaction.save()
        self.assertEqual(models.Saving.objects.get(id=saving.id).balance, Money(110, 'IDR'))

    def test_update_transaction_out_increase_saving(self):
        user = core_factories.UserFactory()
        saving = factories.SavingFactory(owner=user, is_default=True, balance=100)

        transaction = models.Transaction.objects.create(
                description="Dummy transaction",
                date=timezone.now(),
                transaction_type='out',
                amount=20,
                entry_by=user,
                saving=saving
                )
        self.assertEqual(models.Transaction.objects.get(id=transaction.id).saving, saving)
        self.assertEqual(models.Saving.objects.get(id=saving.id).balance, Money(80, 'IDR'))

        transaction = models.Transaction.objects.get(id=transaction.id)
        transaction.amount = 10
        transaction.save()
        self.assertEqual(models.Saving.objects.get(id=saving.id).balance, Money(90, 'IDR'))

    def test_update_remove_transaction_saving_in_return_saving(self):
        user = core_factories.UserFactory()
        saving = factories.SavingFactory(owner=user, is_default=True, balance=100)

        transaction = models.Transaction.objects.create(
                description="Dummy transaction",
                date=timezone.now(),
                transaction_type='in',
                amount=20,
                entry_by=user,
                saving=saving
                )
        self.assertEqual(models.Transaction.objects.get(id=transaction.id).saving, saving)
        self.assertEqual(models.Saving.objects.get(id=saving.id).balance, Money(120, 'IDR'))

        transaction = models.Transaction.objects.get(id=transaction.id)
        transaction.saving = None
        transaction.save()
        self.assertEqual(models.Saving.objects.get(id=saving.id).balance, Money(100, 'IDR'))

    def test_update_remove_transaction_saving_out_return_saving(self):
        user = core_factories.UserFactory()
        saving = factories.SavingFactory(owner=user, is_default=True, balance=100)

        transaction = models.Transaction.objects.create(
                description="Dummy transaction",
                date=timezone.now(),
                transaction_type='out',
                amount=20,
                entry_by=user,
                saving=saving
                )
        self.assertEqual(models.Transaction.objects.get(id=transaction.id).saving, saving)
        self.assertEqual(models.Saving.objects.get(id=saving.id).balance, Money(80, 'IDR'))

        transaction = models.Transaction.objects.get(id=transaction.id)
        transaction.saving = None
        transaction.save()
        self.assertEqual(models.Saving.objects.get(id=saving.id).balance, Money(100, 'IDR'))

    def test_delete_transaction_in_increase_saving(self):
        user = core_factories.UserFactory()
        saving = factories.SavingFactory(owner=user, is_default=True, balance=100)

        transaction = models.Transaction.objects.create(
                description="Dummy transaction",
                date=timezone.now(),
                transaction_type='in',
                amount=20,
                entry_by=user,
                saving=saving
                )
        self.assertEqual(models.Transaction.objects.get(id=transaction.id).saving, saving)
        self.assertEqual(models.Saving.objects.get(id=saving.id).balance, Money(120, 'IDR'))

        transaction = models.Transaction.objects.get(id=transaction.id)
        transaction.delete()
        self.assertEqual(models.Saving.objects.get(id=saving.id).balance, Money(100, 'IDR'))

    def test_delete_transaction_out_decrease_saving(self):
        user = core_factories.UserFactory()
        saving = factories.SavingFactory(owner=user, is_default=True, balance=100)

        transaction = models.Transaction.objects.create(
                description="Dummy transaction",
                date=timezone.now(),
                transaction_type='out',
                amount=20,
                entry_by=user,
                saving=saving
                )
        self.assertEqual(models.Transaction.objects.get(id=transaction.id).saving, saving)
        self.assertEqual(models.Saving.objects.get(id=saving.id).balance, Money(80, 'IDR'))

        transaction = models.Transaction.objects.get(id=transaction.id)
        transaction.delete()
        self.assertEqual(models.Saving.objects.get(id=saving.id).balance, Money(100, 'IDR'))

    def test_save_transaction_in_increase_saving(self):
        user = core_factories.UserFactory()
        saving = factories.SavingFactory(owner=user, is_default=True, balance=100)

        transaction = models.Transaction.objects.create(
                description="Dummy transaction",
                date=timezone.now(),
                transaction_type='in',
                amount=20,
                entry_by=user,
                saving=saving
                )
        self.assertEqual(models.Transaction.objects.get(id=transaction.id).saving, saving)
        self.assertEqual(models.Saving.objects.get(id=saving.id).balance, Money(120, 'IDR'))

    def test_save_transaction_out_decrease_saving(self):
        user = core_factories.UserFactory()
        saving = factories.SavingFactory(owner=user, is_default=True, balance=100)

        transaction = models.Transaction.objects.create(
                description="Dummy transaction",
                date=timezone.now(),
                transaction_type='out',
                amount=20,
                entry_by=user,
                saving=saving
                )
        self.assertEqual(models.Transaction.objects.get(id=transaction.id).saving, saving)
        self.assertEqual(models.Saving.objects.get(id=saving.id).balance, Money(80, 'IDR'))


    def test_change_transaction_saving_in_move_saving(self):
        user = core_factories.UserFactory()
        saving1 = factories.SavingFactory(name='Saving One', owner=user, is_default=True, balance=100)
        saving2 = factories.SavingFactory(name='Saving Two', owner=user, is_default=False, balance=100)

        transaction = models.Transaction.objects.create(
                description="Dummy transaction",
                date=timezone.now(),
                transaction_type='in',
                amount=20,
                entry_by=user,
                saving=saving1
                )
        self.assertEqual(models.Transaction.objects.get(id=transaction.id).saving, saving1)
        self.assertEqual(models.Saving.objects.get(id=saving1.id).balance, Money(120, 'IDR'))
        self.assertEqual(models.Saving.objects.get(id=saving2.id).balance, Money(100, 'IDR'))

        transaction.saving = saving2
        transaction.save()
        self.assertEqual(models.Transaction.objects.get(id=transaction.id).saving, saving2)
        self.assertEqual(models.Saving.objects.get(id=saving1.id).balance, Money(100, 'IDR'))
        self.assertEqual(models.Saving.objects.get(id=saving2.id).balance, Money(120, 'IDR'))


    def test_change_transaction_saving_out_move_saving(self):
        user = core_factories.UserFactory()
        saving1 = factories.SavingFactory(name='Saving One', owner=user, is_default=True, balance=100)
        saving2 = factories.SavingFactory(name='Saving Two', owner=user, is_default=False, balance=100)

        transaction = models.Transaction.objects.create(
                description="Dummy transaction",
                date=timezone.now(),
                transaction_type='out',
                amount=20,
                entry_by=user,
                saving=saving1
                )
        self.assertEqual(models.Transaction.objects.get(id=transaction.id).saving, saving1)
        self.assertEqual(models.Saving.objects.get(id=saving1.id).balance, Money(80, 'IDR'))
        self.assertEqual(models.Saving.objects.get(id=saving2.id).balance, Money(100, 'IDR'))

        transaction.saving = saving2
        transaction.save()
        self.assertEqual(models.Transaction.objects.get(id=transaction.id).saving, saving2)
        self.assertEqual(models.Saving.objects.get(id=saving1.id).balance, Money(100, 'IDR'))
        self.assertEqual(models.Saving.objects.get(id=saving2.id).balance, Money(80, 'IDR'))

