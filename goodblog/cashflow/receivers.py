from django.db.models import F


def update_saving_after_transaction_delete(sender, **kwargs):
    if kwargs['instance'].saving is not None and kwargs['instance'].transaction_type == 'out':
        kwargs['instance'].saving.balance = F('balance') + kwargs['instance'].amount
        kwargs['instance'].saving.save()
    elif kwargs['instance'].saving is not None and kwargs['instance'].transaction_type == 'in':
        kwargs['instance'].saving.balance = F('balance') - kwargs['instance'].amount
        kwargs['instance'].saving.save()
