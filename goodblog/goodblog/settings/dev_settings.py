import os
import tempfile

from .settings import *

SESSION_ENGINE = "django.contrib.sessions.backends.file"
SESSION_FILE_PATH = tempfile.gettempdir()

INSTALLED_APPS.append('debug_toolbar')
MIDDLEWARE.insert(1, 'debug_toolbar.middleware.DebugToolbarMiddleware')

ALLOWED_HOSTS = ['*']

INTERNAL_IPS = [
    '127.0.0.1',
]

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
        'LOCATION': 'unique-snoflake',
    }
}
