import os

from .prod_settings import *


CSRF_COOKIE_SECURE = True
SESSION_COOKIE_SECURE = True

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST_PASSWORD = os.environ['GOODBLOG_EMAIL_HOST_PASSWORD']
EMAIL_HOST = os.environ['GOODBLOG_EMAIL_HOST']
EMAIL_PORT = os.environ['GOODBLOG_EMAIL_PORT']
EMAIL_HOST_USER = os.environ['GOODBLOG_EMAIL_HOST_USER']
EMAIL_USE_TLS = os.environ['GOODBLOG_EMAIL_USE_TLS']
