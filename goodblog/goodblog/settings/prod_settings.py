import os

import sentry_sdk
from sentry_sdk.integrations.django import DjangoIntegration
from sentry_sdk.integrations.redis import RedisIntegration

from .settings import *

ALLOWED_HOSTS = [os.environ['GOODBLOG_ALLOWED_HOST']]
DEBUG = False
SECRET_KEY = os.environ['GOODBLOG_SECRET_KEY']
STATIC_ROOT = os.path.join(BASE_DIR, 'static')

CSRF_COOKIE_SECURE = True
SESSION_COOKIE_SECURE = True
SESSION_ENGINE = "django.contrib.sessions.backends.cache"

SECURE_REFERRER_POLICY = [
    'strict-origin-when-cross-origin',
]

CACHES = {
    "default": {
        "BACKEND": "django_redis.cache.RedisCache",
        "LOCATION": "redis://127.0.0.1:6379/1",
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient",
        }
    }
}

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': os.environ['GOODBLOG_DATABASE_NAME'],
        'USER': os.environ['GOODBLOG_DATABASE_USER'],
        'PASSWORD': os.environ['GOODBLOG_DATABASE_PASSWORD'],
        'HOST': os.environ['GOODBLOG_DATABASE_HOST'],
        'PORT': os.environ['GOODBLOG_DATABASE_PORT'],
    }
}

if os.environ.get('SENTRY_DSN', None):
    sentry_sdk.init(
            dsn=os.environ['SENTRY_DSN'],
            integrations=[DjangoIntegration(), RedisIntegration()],
            send_default_pii=True,
            )

