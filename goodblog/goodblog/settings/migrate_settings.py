import os

from .settings import *

DEBUG = False

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': os.environ['GOODBLOG_DATABASE_NAME'],
        'USER': os.environ['GOODBLOG_DATABASE_USER'],
        'PASSWORD': os.environ['GOODBLOG_DATABASE_PASSWORD'],
        'HOST': os.environ['GOODBLOG_DATABASE_HOST'],
        'PORT': os.environ['GOODBLOG_DATABASE_PORT'],
    }
}
