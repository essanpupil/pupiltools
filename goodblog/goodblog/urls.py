from django.conf import settings
from django.contrib import admin
from django.contrib.sitemaps.views import sitemap
from django.urls import path, include, re_path

from rest_framework.routers import DefaultRouter

from blog import sitemaps
from djcore import views as djcore_views

router = DefaultRouter()

sitemap_info = {
    'blog': sitemaps.PostSitemap,
}

urlpatterns = [
    path('admin/', admin.site.urls),
    path('accounts/', include('allauth.urls')),
    re_path(r'^tinymce/', include('tinymce.urls')),

    path('sitemap.xml', sitemap, {'sitemaps': sitemap_info},
         name='django.contrib.sitemaps.views.sitemap'),

    path('intro', djcore_views.intro, name='intro'),
    path('<slug>', djcore_views.UserDetail.as_view(), name='user_profile'),

    path('blog/', include('blog.urls')),
    path('cashflow/', include('cashflow.urls')),
    path('', djcore_views.HomePage.as_view(), name='home'),
]

urlpatterns += [
    re_path(r'^captcha/', include('captcha.urls')),
]

urlpatterns += [
    path('api/v1/', include(router.urls)),
    path('api-auth/', include('rest_framework.urls')),
]

if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [
        path('__debug__/', include(debug_toolbar.urls)),

        # For django versions before 2.0:
        # url(r'^__debug__/', include(debug_toolbar.urls)),

    ] + urlpatterns
