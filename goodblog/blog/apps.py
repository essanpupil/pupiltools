from django.apps import AppConfig
from django.db.models.signals import post_save


def set_user_level(sender, **kwargs):
    from . import models
    user = kwargs['instance']
    is_created = kwargs['created']
    if is_created:
        user_profile = models.UserProfile(user=user)
        user_profile.save()


class BlogConfig(AppConfig):
    name = 'blog'

    def ready(self):
        from django.contrib.auth.models import User
        post_save.connect(set_user_level, sender=User)
