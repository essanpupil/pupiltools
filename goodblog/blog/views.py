from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.utils import timezone
from django.utils.text import slugify
from django.views.generic.edit import CreateView, UpdateView
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import UserPassesTestMixin
from django.contrib.postgres.search import SearchVector
from django.shortcuts import get_object_or_404
from django.http import Http404

from rest_framework import viewsets, permissions, renderers
from taggit.models import Tag

from . import models, forms, serializers
from .permissions import IsOwnerOrReadOnly


class TestPostAuthor(UserPassesTestMixin):
    def test_func(self):
        return bool(self.request.user.userprofile.level < 3)


class PostDetail(DetailView):
    model = models.Post
    template_name = 'blog/post-detail.html'
    pk_url_kwarg = 'pk'
    slug_url_kwarg = 'title'

    def get_object(self, *args, **kwargs):
        this_post = super(PostDetail, self).get_object(*args, **kwargs)
        if this_post.publish_date is None or this_post.publish_date > timezone.now():
            if this_post.author != self.request.user:
                raise Http404

            return this_post
        else:
            return this_post

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if self.request.user.is_authenticated:
            context['comment_form'] = forms.CommentForm()
        context['comments'] = models.Comment.objects.filter(
                post=self.get_object()).order_by('-publish_date')
        context['tags'] = Tag.objects.all()
        return context

    def post(self, request, *args, **kwargs):
        comment_form = forms.CommentForm(self.request.POST)
        if comment_form.is_valid():
            comment = comment_form.save(commit=False)
            comment.post = self.get_object()
            comment.author = self.request.user
            comment.save()
            return redirect(reverse_lazy('blog:post-detail',
                args=[self.get_object().id, slugify(self.get_object().title)]))
        else:
            return self.get(request, *args, **kwargs)


@method_decorator(login_required, name='dispatch')
class PostUpdate(TestPostAuthor, UpdateView):
    model = models.Post
    template_name = 'blog/post-update.html'
    form_class = forms.PostForm

    def get_success_url(self):
        return reverse_lazy('blog:post-detail', args=[self.object.id, slugify(self.object.title)])


@method_decorator(login_required, name='dispatch')
class PostView(TestPostAuthor, CreateView):
    model = models.Post
    template_name = "blog/new-post.html"
    form_class = forms.PostForm

    def get_success_url(self, *args, **kwargs):
        return reverse_lazy('blog:post-detail', args=[self.object.id, slugify(self.object.title)])


    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)


@method_decorator(login_required, name='dispatch')
class MyPostsView(TestPostAuthor, ListView):
    model = models.Post
    template_name = "blog/my-posts.html"

    def get_queryset(self):
        return super().get_queryset().filter(author=self.request.user).order_by('-publish_date')

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['time_now'] = timezone.now()
        return context


@login_required
def publish_post(request, post_id):
    post = get_object_or_404(models.Post, pk=post_id)
    post.publish_date = timezone.now()
    post.save()
    return redirect('blog:user-posts')


@login_required
def unpublish_post(request, post_id):
    post = get_object_or_404(models.Post, pk=post_id)
    post.publish_date = None
    post.save()
    return redirect('blog:user-posts')


class SearchPostView(ListView):
    model = models.Post
    template_name = 'blog/search-post.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['tags'] = Tag.objects.all()
        return context

    def get_queryset(self):
        query = self.request.GET.get('q')
        object_list = self.model.objects.annotate(
                search=SearchVector('title', 'content'),
                ).filter(
                        publish_date__lte=timezone.now(), search=query
                ).select_related('author').prefetch_related('tags').order_by('-publish_date')
        return object_list


def tag_posts(request, tag_name):
    posts = models.Post.objects.filter(tags__name=tag_name)
    tags = Tag.objects.all()
    return render(request, 'blog/tag-posts.html',
            {
                'post_list': posts,
                'tags': tags,
            })


class PostViewSet(viewsets.ModelViewSet):
    queryset = models.Post.objects.all()
    serializer_class = serializers.PostSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly, IsOwnerOrReadOnly]

    def perform_create(self, serializer):
        serializer.save(author=self.request.user)
