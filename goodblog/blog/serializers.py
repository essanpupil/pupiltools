from rest_framework import serializers
from taggit_serializer.serializers import TaggitSerializer, TagListSerializerField

from . import models


class PostSerializer(TaggitSerializer, serializers.HyperlinkedModelSerializer):
    author = serializers.ReadOnlyField(source='author.username')
    tags = TagListSerializerField()

    class Meta:
        model = models.Post
        fields = ['id', 'url', 'title', 'create_date', 'publish_date',
                  'author', 'tags', 'content']
