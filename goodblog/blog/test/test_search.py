from unittest import skipUnless

from django.conf import settings
from django.test import TestCase, Client
from django.urls import reverse


class SearchPostTest(TestCase):
    client = Client()

    @skipUnless(hasattr(settings, 'POSTGRESDB'), "Test database does not support search feature")
    def test_search(self):
        response = self.client.get(reverse('blog:search-post'), {'q': ''})

