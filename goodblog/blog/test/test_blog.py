from datetime import timedelta
from django.test import LiveServerTestCase, Client
from django.urls import reverse
from django.utils.text import slugify
from django.utils import timezone

from blog.test.factories import PostFactory

from djcore.tests.factories import UserFactory



class PostDetailTest(LiveServerTestCase):
    client = Client()

    def test_future_scheduled_post_detail_logged_in_author(self):
        user = UserFactory()
        user.userprofile.level = 2
        user.userprofile.save()
        self.client.force_login(user)
        post = PostFactory(
                title='post Testing', publish_date=timezone.now() + timedelta(minutes=5),
                author=user)
        response = self.client.get(
                reverse('blog:post-detail',
                kwargs={'pk': post.id, 'title': slugify(post.title)}
                ))
        self.assertEqual(response.status_code, 200)

    def test_draft_post_detail_logged_in_author(self):
        user = UserFactory()
        user.userprofile.level = 2
        user.userprofile.save()
        self.client.force_login(user)
        post = PostFactory(title='post Testing', publish_date=None, author=user)
        response = self.client.get(
                reverse('blog:post-detail',
                kwargs={'pk': post.id, 'title': slugify(post.title)}
                ))
        self.assertEqual(response.status_code, 200)

    def test_future_scheduled_post_detail_logged_in_user(self):
        user = UserFactory()
        self.client.force_login(user)
        post = PostFactory(
                title='post Testing', publish_date=timezone.now() + timedelta(minutes=5))
        response = self.client.get(
                reverse('blog:post-detail',
                kwargs={'pk': post.id, 'title': slugify(post.title)}
                ))
        self.assertEqual(response.status_code, 404)

    def test_draft_post_detail_logged_in_user(self):
        user = UserFactory()
        self.client.force_login(user)
        post = PostFactory(title='post Testing', publish_date=None)
        response = self.client.get(
                reverse('blog:post-detail',
                kwargs={'pk': post.id, 'title': slugify(post.title)}
                ))
        self.assertEqual(response.status_code, 404)

    def test_future_schedul_post_detail_not_logged_in(self):
        post = PostFactory(
                title='post Testing', publish_date=timezone.now() + timedelta(minutes=5))
        response = self.client.get(
                reverse('blog:post-detail',
                kwargs={'pk': post.id, 'title': slugify(post.title)}
                ))
        self.assertEqual(response.status_code, 404)

    def test_draft_post_detail_not_logged_in(self):
        post = PostFactory(title='post Testing', publish_date=None)
        response = self.client.get(
                reverse('blog:post-detail',
                kwargs={'pk': post.id, 'title': slugify(post.title)}
                ))
        self.assertEqual(response.status_code, 404)

    def test_post_detail_not_logged_in(self):
        post = PostFactory(title='post Testing', publish_date=timezone.now())
        response = self.client.get(
                reverse('blog:post-detail',
                kwargs={'pk': post.id, 'title': slugify(post.title)}
                ))
        self.assertEqual(response.status_code, 200)

