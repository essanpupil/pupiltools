from django.test import TestCase, Client
from django.urls import reverse

from blog.test.factories import PostFactory

class TagPostsTest(TestCase):
    client = Client()

    def test_tag_posts(self):
        post = PostFactory()
        post.tags = 'testing'
        post.save()
        response = self.client.get(reverse('blog:tag-posts', args=[post.tags]))
        self.assertEqual(response.status_code, 200)

