import factory

from djcore.tests import factories

from blog.models import Post


class PostFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Post

    author = factory.SubFactory(factories.UserFactory)
