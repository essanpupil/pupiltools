from django.test import LiveServerTestCase, Client
from django.test.client import RequestFactory
from django.urls import reverse, reverse_lazy
from django.utils import timezone
from django.utils.text import slugify

from blog.test.factories import PostFactory
from blog.models import Comment
from blog.views import PostDetail

from djcore.tests.factories import UserFactory


class CommentTest(LiveServerTestCase):
    client = Client()

    def test_add_new_commment(self):
        user = UserFactory()
        post = PostFactory(title='post Testing', publish_date=timezone.now())
        self.client.login(username='fulan', password='testpassword')
        response = self.client.post(
                reverse('blog:post-detail',
                    kwargs={'pk': post.id, 'title': slugify(post.title)}),
                {'content': 'Nice Post'},
                )

        self.assertEqual(1, Comment.objects.count())


    def test_add_empty_commment(self):
        user = UserFactory()
        self.client.force_login(user)
        post = PostFactory(title='post Testing', publish_date=timezone.now())
        response = self.client.post(
                reverse('blog:post-detail',
                    kwargs={'pk': post.id, 'title': slugify(post.title)}),
                {'content': ''},
                )
        self.assertEqual(0, Comment.objects.count())

