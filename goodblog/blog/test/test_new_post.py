from django.test import TestCase, Client
from django.urls import reverse

from blog.models import Post

from djcore.tests.factories import UserFactory


class NewPostTest(TestCase):
    client = Client()

    def test_new_post_not_logged_in_redirected(self):
        response = self.client.get(reverse('blog:new-post'))
        self.assertEqual(response.status_code, 302)

    def test_new_post_logged_in_author(self):
        fulan = UserFactory()
        fulan.userprofile.level = 1
        fulan.userprofile.save()
        self.client.force_login(fulan)

        response = self.client.get(reverse('blog:new-post'))
        self.assertEqual(response.status_code, 200)

    def test_new_post_logged_in_non_author(self):
        fulan = UserFactory()
        self.client.force_login(fulan)

        response = self.client.get(reverse('blog:new-post'))
        self.assertEqual(response.status_code, 403)

    def test_save_new_post(self):
        fulan = UserFactory()
        fulan.userprofile.level = 1
        fulan.userprofile.save()
        self.client.force_login(fulan)

        post_data = {
                'title': 'Post Testing',
                'content': 'lorem ipsum met dorole',
                'tags': 'test',
                }
        response = self.client.post(reverse('blog:new-post'), post_data, follow=True)
        self.assertFalse(response.context.get('form'))
        self.assertTrue(Post.objects.get(title=post_data['title']))

