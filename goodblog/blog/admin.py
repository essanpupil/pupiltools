from django.contrib import admin

from . import models

@admin.register(models.UserProfile)
class ProfileModelAdmin(admin.ModelAdmin):
    list_display = ('user', 'level')
    readonly_fields=('user',)

