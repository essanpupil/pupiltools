from django import forms

from tinymce.widgets import TinyMCE

from . import models


class PostForm(forms.ModelForm):
    id = forms.CharField(widget=forms.HiddenInput(), disabled=True, required=False)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['tags'].widget.attrs.update({'class': 'input'})

    class Meta:
        model = models.Post
        fields = ['id', 'title', 'tags', 'publish_date', 'content']
        widgets = {
            'title': forms.TextInput(attrs={'class': 'input'}),
            'publish_date': forms.TextInput(attrs={'class': 'input'}),
            'content': TinyMCE()
        }


class CommentForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['content'].widget.attrs.update({'class': 'textarea is-info is-small'})

    class Meta:
        model = models.Comment
        fields = ['content']
