from datetime import datetime

from django.contrib.sitemaps import Sitemap
from django.utils import timezone

from . import models

class PostSitemap(Sitemap):
    changefreq = "never"
    priority = 0.5

    def items(self):
        return models.Post.objects.filter(publish_date__lte=timezone.now())

    def lastmod(self, obj):
        return obj.publish_date
