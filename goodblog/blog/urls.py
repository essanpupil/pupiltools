from django.urls import path

from goodblog.urls import router
from . import views


router.register(r'blog/articles', views.PostViewSet)

app_name='blog'
urlpatterns = [
    path('new-post/', views.PostView.as_view(), name='new-post'),
    path('my-posts/', views.MyPostsView.as_view(), name='user-posts'),
    path('search/', views.SearchPostView.as_view(), name='search-post'),
    path('tag/<tag_name>', views.tag_posts, name='tag-posts'),
    path('article/<pk>/<slug:title>', views.PostDetail.as_view(), name='post-detail'),
    path('update-article/<pk>', views.PostUpdate.as_view(), name='post-update'),
    path('publish-article/<post_id>', views.publish_post, name='publish-post'),
    path('unpublish-article/<post_id>', views.unpublish_post, name='unpublish-post'),
]
