flatpickr("#id_publish_date",
    {
        enableTime: true,
        plugins: [new confirmDatePlugin({})]
    }
)

var id_post = document.getElementById('id_id');

function tagParser() {
    var tags = document.getElementById('id_tags').value;
    if (tags.indexOf(',') > -1) {
        return tags.split(' ');
    } else {
        return tags.split(',');
    }
}

function checkFormValidity() {
    var id_post = document.getElementById('id_id');
    var title = document.getElementById('id_title').value;
    var publish_date = document.getElementById('id_publish_date').value;
    var tags = tagParser();
    var content = tinymce.activeEditor.getContent();
    if (title !== '' && tags !== [] && content !== '') {
        if (id_post.value === '') {
            createPost(JSON.stringify({
                title: title,
                tags: tags,
                publish_date: publish_date !== '' ? publish_date : null,
                content: content,
            }))    
        } else {
            updatePost(JSON.stringify({
                title: title,
                tags: tags,
                publish_date: publish_date !== '' ? publish_date : null,
                content: content,
            }), id_post.value)
        }
    }
}

function createPost(form_data) {
    var url = window.location.protocol + '//' + window.location.host + '/api/v1/blog/articles/';
    savePost(form_data, url, 'POST');
}

function updatePost(form_data, id_post) {
    var url = window.location.protocol + '//' + window.location.host + '/api/v1/blog/articles/' + id_post + '/';
    savePost(form_data, url, 'PUT');
}

function savePost(form_data, url, method) {
    fetch(url, {
        method: method,
        body: form_data,
        credentials: 'include',
        headers: {
            'Content-Type': 'application/json; charset=UTF-8',
            'X-CSRFToken': getCookie('csrftoken'),
        }
    }).then(function (response) {
        if (response.ok) {
            return response.json();
        }
        return Promise.reject(response);
    }).then(function (data) {
        id_post.value = data.id;
        var postForm = document.getElementById('post-form').action = window.location.protocol + '//' + window.location.host + '/blog/update-article/' + id_post.value;
    }).catch(function (error) {
        console.warn('Something went wrong.', error);
    });
}

const autosave = setInterval(checkFormValidity, 60000);
