from datetime import datetime

from django.contrib.auth.models import User
from django.db import models
from django.urls import reverse_lazy
from django.template.defaultfilters import slugify

from tinymce.models import HTMLField
from taggit.managers import TaggableManager


class UserProfile(models.Model):
    USER_LEVEL_OPTIONS = [
        (1, 'Administrator'),
        (2, 'Author'),
        (3, 'Reader'),
    ]
    user = models.OneToOneField(User, on_delete=models.CASCADE, editable=False)
    level = models.SmallIntegerField(choices=USER_LEVEL_OPTIONS, default=3)

    def __str__(self):
        return str(self.level)


class Post(models.Model):
    title = models.CharField(max_length=255, unique=True)
    create_date = models.DateTimeField(auto_now_add=True)
    publish_date = models.DateTimeField(null=True, blank=True, help_text="yyyy-mm-dd hh:mm:ss")
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    tags = TaggableManager()
    content = HTMLField()

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse_lazy('blog:post-detail', args=[self.id, slugify(self.title)])


class Comment(models.Model):
    post = models.ForeignKey(Post, on_delete=models.CASCADE)
    publish_date = models.DateTimeField(auto_now_add=True)
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    content = HTMLField()

    def __str__(self):
        return self.publish_date.strftime("%d %B %Y, %H:%M")
