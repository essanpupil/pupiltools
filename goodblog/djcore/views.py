from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.db.models import Prefetch
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from django.shortcuts import render
from django.utils.decorators import method_decorator
from django.utils import timezone

from taggit.models import Tag

from blog.models import Post
from djcore.models import Core


@method_decorator(login_required, name='dispatch')
class UserDetail(DetailView):
    model = User
    slug_field = 'username'
    template_name = 'djcore/user-detail.html'


def intro(request):
    core = Core.objects.prefetch_related(
            Prefetch('contact_set')).prefetch_related(
                    Prefetch('service_set')).prefetch_related(
                            Prefetch('portfolio_set')).first()
    return render(request, 'djcore/intro.html', {'core': core})


class HomePage(ListView):
    model = Post
    paginate_by = 7
    template_name = 'blog/home.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['tags'] = Tag.objects.all()
        return context

    def get_queryset(self):
        return self.model.objects.filter(publish_date__lte=timezone.now()).select_related('author'
            ).prefetch_related('tags').order_by('-publish_date')[:7]
