from django.test import LiveServerTestCase, Client

from . import factories


class BaseTest(LiveServerTestCase):

    def setUp(self):
        self.client = Client()

    def create_user(self):
        user = factories.UserFactory()
        return user

    def user_login(self, user, passwd):
        return self.client.login(username=user.username, password=passwd)
