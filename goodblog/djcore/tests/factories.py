from django.contrib.auth import models

import factory


class UserFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = models.User
        django_get_or_create = ('username',)

    username = 'fulan'
    password = factory.PostGenerationMethodCall('set_password', 'testpassword')
