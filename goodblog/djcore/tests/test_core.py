from django.urls import reverse

from .base import BaseTest
from .factories import UserFactory


class HomePageTest(BaseTest):
    def test_home(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)


class AuthenticationTest(BaseTest):
    def test_login_page(self):
        response = self.client.get('/accounts/login/')
        self.assertEqual(response.status_code, 200)

    def test_signup_page(self):
        response = self.client.get('/accounts/signup/')
        self.assertEqual(response.status_code, 200)

    def test_logout_page(self):
        fulan = UserFactory()
        self.client.force_login(fulan)
        response = self.client.get('/accounts/logout/')
        self.assertEqual(response.status_code, 200)

    def test_password_reset(self):
        response = self.client.get('/accounts/password/reset/')
        self.assertEqual(response.status_code, 200)


class IntroTest(BaseTest):
    def test_intro(self):
        response = self.client.get(reverse('intro'))
        self.assertEqual(response.status_code, 200)


class UserProfileTest(BaseTest):
    def test_user_profile(self):
        fulan = UserFactory()
        self.client.force_login(fulan)
        response = self.client.get('/fulan')
        self.assertEqual(response.status_code, 200)
