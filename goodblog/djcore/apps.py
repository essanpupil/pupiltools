from django.apps import AppConfig


class DjcoreConfig(AppConfig):
    name = 'djcore'
