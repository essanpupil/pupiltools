from django.contrib.sites.models import Site
from django.db import models
from django.utils.translation import gettext_lazy as _


class SiteConfig(models.Model):
    site = models.OneToOneField(Site, on_delete=models.CASCADE)
    home_page_title = models.CharField(max_length=128, null=True, blank=True)
    show_intro_menu = models.BooleanField(default=False)

    def __str__(self):
        return self.home_page_title

    def save(self, *args, **kwargs):
        Site.objects.clear_cache()
        super(SiteConfig, self).save(*args, **kwargs)


class Core(models.Model):
    name = models.CharField(max_length=128, default='GoodBlog')
    slogan = models.CharField(max_length=128, null=True, blank=True)
    address = models.TextField(null=True, blank=True)
    detail = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.name


class Contact(models.Model):

    class ContactType(models.TextChoices):
        EMAIL = 'email', _('Email')
        PHONE = 'phone', _('Phone')
        FAX = 'fax', _('Fax')
        MOBILE_PHONE = 'mobile', _('Mobile Phone')

    name = models.CharField(max_length=16, choices=ContactType.choices, null=True, blank=True)
    content = models.CharField(max_length=64)
    core = models.ForeignKey(Core, on_delete=models.CASCADE, null=True, blank=True)

    def __str__(self):
        return self.name


class Service(models.Model):
    name = models.CharField(max_length=64)
    overview = models.TextField(null=True, blank=True)
    detail = models.TextField(null=True, blank=True)
    core = models.ForeignKey(Core, on_delete=models.CASCADE, null=True, blank=True)

    def __str__(self):
        return self.name


class Portfolio(models.Model):
    name = models.CharField(max_length=128)
    webpage = models.URLField(null=True, blank=True)
    overview = models.TextField(null=True, blank=True)
    detail = models.TextField(null=True, blank=True)
    core = models.ForeignKey(Core, on_delete=models.CASCADE, null=True, blank=True)

    def __str__(self):
        return self.name
