from django.contrib import admin

from .models import Core, Portfolio, Service, Contact, SiteConfig


admin.site.register(Core)
admin.site.register(Portfolio)
admin.site.register(Service)
admin.site.register(Contact)
admin.site.register(SiteConfig)

