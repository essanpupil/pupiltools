## [0.0.13] 2020-06-12
### Add
- Filter transaction list.
- Next and previous button for transaction list.


## [0.0.12] 2020-06-08
### Add
- Edit savings in cashflow app.
- Delete savings in cashflow app.


## [0.0.11] 2020-06-01
### Add
- Edit transaction in cashflow app.
- Delete transaction in cashflow app.


## [0.0.10] 2020-05-29
### Add
- New application cashflow to record cash usage.


## [0.0.9] 2020-05-21
### Add
- Api endpoints for blog app.
- Autosave when creating and updating blog post.
- Tags list in post fdetail page.

## Remove
- Unused image from base template.

## Fix
- Logging config, get more meaningfull information.

## Chore
- Remove unused import statement.


## [0.0.8] 2020-05-07
### Add
- Page to display post based-on tag
- Tag list in home
- Sentry logging for warning and error events

### Remove
- Custom js for tinymce, we will use js files from package django-tinymce

### Upgrade
- django to version 3.0.6
- django-tinymce to version 3.0.2


## [0.0.7] 2020-05-03
### Add
- Site config model. This model has one to one relation to Site model fom django built in Site framework. This model will be used as feature toggle.
- Intro page. This page has feature toggle located in admin page, in models site config. This page is intented for profile page of this website.

### Chore
- Refactor core templates & tests from blog app to djcore app. The intention is to make blog app only store modules & files related to blog functionality.


## [0.0.6] 2020-04-30
### Add
- Search blog article based on title & tezt in content

### Chore
- Increase test coverage for page new post


## [0.0.5] 2020-04-28
### Refactor
- Psycopg2, replace it with psycopg2-binary to reduce installed OS package.
- Fontawesome js, replace with fontawesome css to reduce statis file size.
- Js script for post update & post create, put it in one static file.

### Add
- Default blank for post publish date.
- Filter to hide draft post in home page.
- Flatpickr as datetime widget.
- Publish & unpublish tools in post manajemen.
- 404 page when draft post is accessed not by its author.
- Postgresql database in development environment.


## [0.0.4] 2020-04-25 Feature & Fix release
### Added
- 404 page. This page is handled by app, because the app will still works normally in this response code.
- 403 page. This page is handled by app, because the app will still works normally in this response code.
- 5xx page. This page is a static page handled by nginx, because when 5xx, maybe the app is stop working.

### Fixed
- User profile page only display registered user.
- Letsencrypt installation when deploying to aws.


## [0.0.3] 2020-04-21 Feature Release
### Added
- blog post title in post detail page
- /sitemap.xml
- /robots.txt
- goodblogger owner role when creating database
- link to pupil.id in readme

### Fixed
- vagrant hosts inventory for known hosts verification


## [0.0.2] 2020-04-20 Feature Release
### Removed
- Letsencrypt role from aws deployment. At this time, result from letsencrypt role is not like what i expected. It can not configure automatic redirect from http to https.
- Pure css, it is replaced by Bulma css.
- Unused css files remnant of base template copied from pure css tutorial.
- Logout confirmation page.
- SSL config from nginx. At this time, ssl configration is done manually by executing certbot from server.

### Added
- [Bulma css](https://bulma.io/). Pure css is simple & light, but too simple for me to modify my web styling. My front end development experience is green, hence i need more ready made class styling. Bulma css is also simple but relatively have more ready made style class. For me, Bulma css is a good balance between light and features.
- Logout confirmation modal.
- Django app djcore. In the future, this app will store modules & files which seems like does not belong to any other app. This app is supposed to be the core app that exists in all my django project.
- Allow blank value for Post publish date.
- Environment conditional check for email configuration.

### Fixed
- Vagrant deployment, no need to worry about ssh known hosts fingerprint.


## [0.0.1] 2020-04-12 Initial Release
### Added
- User authentication using django-allauth
- Automation scripts for vagrant & AWS server provisioning & deployment
