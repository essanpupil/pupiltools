# goodblog
[![pipeline status](https://gitlab.com/essanpupil/goodblog/badges/master/pipeline.svg)](https://gitlab.com/essanpupil/goodblog/-/commits/master)
[![coverage report](https://gitlab.com/essanpupil/goodblog/badges/master/coverage.svg)](https://gitlab.com/essanpupil/goodblog/-/commits/master)


Source code for my personal website [pupil.id](https://pupil.id "/home/pupil/")
